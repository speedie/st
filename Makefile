# st - simple terminal
# See LICENSE file for copyright and license details.
.POSIX:

include options.mk

SRC = st.c x.c sixel.c sixelhls.c ligatures.c bd.c
OBJ = $(SRC:.c=.o)

all: options st

options:
	@echo st build options:
	@echo "CFLAGS  = $(CFLAGS)"
	@echo "LDFLAGS = $(LDFLAGS)"
	@echo "CC      = $(CC)"

.c.o:
	$(CC) $(CFLAGS) -c $<

st.o: options.h st.h win.h
x.o: arg.h options.h st.h win.h
boxdraw.o: options.h st.h bd.h
ligatures.o: st.h

$(OBJ): options.mk

st: $(OBJ)
	$(CC) -o $@ $(OBJ) $(LDFLAGS)

clean:
	rm -f st $(OBJ) st-$(VERSION).tar.gz

dist: clean
	mkdir -p st-spde-$(VERSION)
	cp -R LICENSE Makefile *.mk \
		*.h *.info *.c *.desktop *.png docs/ scripts/ \
		st-spde-$(VERSION)
	tar -cf - st-spde-$(VERSION) | gzip > st-spde-$(VERSION).tar.gz
	rm -rf st-spde-$(VERSION)

install: st
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f st $(DESTDIR)$(PREFIX)/bin
	cp -f scripts/* $(DESTDIR)$(PREFIX)/bin
	chmod +x $(DESTDIR)$(PREFIX)/bin/st*
	chmod 755 $(DESTDIR)$(PREFIX)/bin/st
	mkdir -p $(DESTDIR)$(APPPREFIX)
	cp -f st.desktop $(DESTDIR)$(APPPREFIX)
	[ -f $(ICONNAME) ] && cp -f $(ICONNAME) $(DESTDIR)$(ICONPREFIX) || :
	rm -f *.o st
	tic -sx st.info

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/st
	rm -f $(DESTDIR)$(PREFIX)/bin/st.desktop

.PHONY: all options clean dist install uninstall

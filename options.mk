# st version
VERSION = 1.1

# Customize below to fit your system

# paths
PREFIX = /usr
APPPREFIX = $(PREFIX)/share/applications
ICONPREFIX = $(PREFIX)/share/pixmaps
ICONNAME = st.png

X11INC = /usr/X11R6/include
X11LIB = /usr/X11R6/lib

PKG_CONFIG = pkg-config

# includes and libs
INCS = -I$(X11INC) \
       `$(PKG_CONFIG) --cflags fontconfig` \
       `$(PKG_CONFIG) --cflags harfbuzz` \
       `$(PKG_CONFIG) --cflags freetype2`
LIBS = -L$(X11LIB) -lm -lrt -lX11 -lutil -lXft -lXrender -lgd \
       `$(PKG_CONFIG) --libs fontconfig` \
       `$(PKG_CONFIG) --libs harfbuzz` \
       `$(PKG_CONFIG) --libs freetype2`

CFLAGS = -std=c99 -pedantic -Wall -Wno-deprecated-declarations -Wno-unused-variable -Wno-unused-result -Wno-unused-function -Ofast -march=native $(INCS) $(CPPFLAGS)


# flags
CPPFLAGS = -DVERSION=\"$(VERSION)\" -DICON=\"$(ICONPREFIX)/$(ICONNAME)\" -D_XOPEN_SOURCE=600
LDFLAGS = $(LIBS) -g


# OpenBSD:
#CPPFLAGS = -DVERSION=\"$(VERSION)\" -D_XOPEN_SOURCE=600 -D_BSD_SOURCE
#LIBS = -L$(X11LIB) -lm -lX11 -lutil -lXft -lgd -lXrender \
#       `$(PKG_CONFIG) --libs fontconfig` \
#       `$(PKG_CONFIG) --libs harfbuzz` \
#       `$(PKG_CONFIG) --libs freetype2`

# compiler and linker
CC = tcc

/* Toggle features */
#define USEXRESOURCES 1 /* Include Xresources support (0/1) */

/* Toggle functionality */
#define RELOADCOLORS  0 /* Allow programs like pywal to reload colors during runtime through sequences (0/1) */
